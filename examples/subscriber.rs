use anyhow::Result;
use gmqtt_client::{Message, MqttClientBuilder, QoS};
use log::{info, LevelFilter};
use tokio::runtime;

const MQTT_BROKER: &str = "tcp://localhost:1883";

fn main() -> Result<()> {
    env_logger::Builder::new()
        .filter_level(LevelFilter::Debug)
        .init();

    let (_, mqtt_worker) = MqttClientBuilder::new(MQTT_BROKER.parse()?)
        .on_connected_callback(on_mqtt_connected)
        .on_message_callback(on_mqtt_message)
        .subscribe("client/example/pubsub", QoS::AtLeastOnce)
        .build();

    let worker_runtime = runtime::Builder::new_multi_thread()
        .enable_all()
        .worker_threads(1)
        .build()?;

    worker_runtime.spawn(async { mqtt_worker.run().await });

    std::thread::park();
    Ok(())
}

fn on_mqtt_connected() {
    info!("MQTT connected");
}

fn on_mqtt_message(message: &Message) {
    println!(
        "MQTT message received: {}",
        String::from_utf8_lossy(message.payload())
    );
}
