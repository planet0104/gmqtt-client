use mqttbytes::v5::SubscribeFilter;

use super::Message;
use std::time::Instant;

pub type OnMessageCallback = dyn FnMut(&Message) + Send + Sync + 'static;
pub type OnMessageOwnedCallback = dyn FnMut(Message, Instant) + Send + 'static;
pub type OnConnectedCallback = dyn Fn() + Send + Sync + 'static;

pub(crate) struct MqttClientConfig {
    pub client_id: String,
    pub subscribe_filters: Vec<SubscribeFilter>,
    pub on_message_callback: Option<Box<OnMessageCallback>>,
    pub on_connected_callback: Option<Box<OnConnectedCallback>>,
    pub clean_session: bool,
}
