use std::{cmp::PartialOrd, num::NonZeroU16};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(super) struct PacketId(NonZeroU16);

impl PacketId {
    pub fn new() -> Self {
        Self(NonZeroU16::new(1).unwrap())
    }

    pub fn increment(&mut self) {
        let (mut pkid, _) = self.0.get().overflowing_add(1);

        if pkid == 0 {
            pkid += 1;
        }

        self.0 = NonZeroU16::new(pkid).unwrap()
    }
}

impl From<PacketId> for u16 {
    fn from(pkid: PacketId) -> Self {
        pkid.0.get()
    }
}

impl PartialEq<u16> for PacketId {
    fn eq(&self, other: &u16) -> bool {
        self.0.get() == *other
    }
}

impl PartialOrd<u16> for PacketId {
    fn partial_cmp(&self, other: &u16) -> Option<std::cmp::Ordering> {
        self.0.get().partial_cmp(other)
    }
}
